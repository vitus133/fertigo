package gitconf_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/vitus133/fertigo/pkg/gitconf"
)

func TestPullSchedule(t *testing.T) {
	sch, err := gitconf.PullSchedule("https://gitlab.com/vitus133/fertigo", "main", "pkg/schedule/test_data/test_schedule.yaml")
	assert.Equal(t, err, nil, "failed to pull schedule")
	assert.Equal(t, 1, len(sch.Lines), "failed to correctly interpret schedule")

}
