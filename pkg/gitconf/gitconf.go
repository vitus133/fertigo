package gitconf

import (
	"fmt"
	"io"
	"strings"

	"gitlab.com/vitus133/fertigo/pkg/schedule"

	"gopkg.in/src-d/go-billy.v4/memfs"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/storage/memory"
	"gopkg.in/yaml.v3"
)

func PullSchedule(repository, branch, path string) (*schedule.Schedule, error) {
	fs := memfs.New()
	store := memory.NewStorage()
	_, err := git.Clone(store, fs, &git.CloneOptions{
		URL:           repository,
		Auth:          nil,
		ReferenceName: plumbing.ReferenceName(fmt.Sprintf("refs/heads/%s", branch)),
		SingleBranch:  true,
		Depth:         1,
	})
	if err != nil {
		return nil, err
	}

	f, err := fs.Open(path)
	if err != nil {
		return nil, err
	}
	var p []byte
	_, err = f.Read(p)
	if err != nil {
		return nil, err
	}
	buf := new(strings.Builder)
	_, err = io.Copy(buf, f)
	if err != nil {
		return nil, err
	}
	var s schedule.Schedule
	err = yaml.Unmarshal([]byte(buf.String()), &s)
	return &s, err
}
