package schedule

import (
	"context"
	"fmt"
	"io/fs"
	"log"
	"os"
	"sync"
	"time"

	rpio "github.com/stianeikeland/go-rpio/v4"
	"gopkg.in/yaml.v3"
)

type FertigationTimeSlot struct {
	Start        string        `yaml:"start"`
	Duration     time.Duration `yaml:"duration"`
	PumpDuration time.Duration `yaml:"pumpDuration,omitempty"`
	// If weekday is omitted, the slot is executed every day
	Weekday string `yaml:"weekday,omitempty"`
}

type Line struct {
	LineId   int                   `yaml:"lineId,omitempty"`
	LineName string                `yaml:"lineName"`
	Slots    []FertigationTimeSlot `yaml:"slots"`
	Mapping  map[string]int        `yaml:"mapping"`
}

type Schedule struct {
	Lines []Line `yaml:"lines"`
}

func ReadScheduleFile(path string) (*Schedule, error) {
	var s Schedule
	file, err := os.ReadFile(path)
	if err != nil {
		return &s, err
	}
	err = yaml.Unmarshal(file, &s)
	return &s, err
}

func (s *Schedule) WriteScheduleFile(path string) error {

	bytes, err := yaml.Marshal(s)
	if err != nil {
		return err
	}
	return os.WriteFile(path, bytes, fs.ModePerm)
}

func (s *Schedule) GetSlot(lineIdx, slotIdx int) (*FertigationTimeSlot, error) {
	if len(s.Lines) <= lineIdx || len(s.Lines[lineIdx].Slots) <= slotIdx {
		return nil, fmt.Errorf("line / slot index error")
	}
	return &s.Lines[lineIdx].Slots[slotIdx], nil
}

func (sl *FertigationTimeSlot) GetStartTime() (*time.Time, error) {
	const timeLayout = "2006-01-02T15:04"
	const dateOnlyLayout = "2006-01-02"

	currentTime := time.Now()
	currentDate := currentTime.Format(dateOnlyLayout)
	startDateStr := currentDate + "T" + sl.Start
	tm, err := time.Parse(timeLayout, startDateStr)
	if err != nil {
		return nil, err
	}
	return &tm, nil
}

func NowToMinute() time.Duration {
	return time.Duration(60-time.Now().Second()) * time.Second
}

func OfflineTickHandler(valve, pump1 bool, mapping map[string]int) {
	log.Printf("slot handler: valve %v, pump1 %v\n", valve, pump1)
}

func InitIo(mapping map[string]int) error {
	err := rpio.Open()
	if err != nil {
		log.Println("failed to open gpio")
		return err
	}

	for _, v := range mapping {
		pin := rpio.Pin(v)
		log.Println("setting high pin", v)
		pin.Output()
		pin.Write(rpio.High)
	}
	return nil
}

func TickHandler(valve bool, pump1 bool, mapping map[string]int) {
	if valve {
		rpio.Pin((mapping)["valve"]).Low()
	} else {
		rpio.Pin((mapping)["valve"]).High()
	}
	if pump1 {
		rpio.Pin((mapping)["pump"]).Low()
	} else {
		rpio.Pin((mapping)["pump"]).High()
	}
}

func DoSlot(ctx context.Context,
	wg *sync.WaitGroup,
	lineName string,
	lineMapping map[string]int,
	slot FertigationTimeSlot,
	tickDuration time.Duration,
	tickHandler func(valve, pump1 bool, mapping map[string]int)) {

	numTicks := int(slot.Duration / tickDuration)
	pTicks := int(slot.PumpDuration / tickDuration)
	ticker := time.NewTicker(tickDuration)
	// There will be one pump tick per pump duty cycle ticks
	pumpDutyCycle := numTicks / pTicks
	// Compensate valve ticks for pump ticks, as we are going to shut the valve when
	// the pump is working
	numTicks += pTicks
	numTicksInCycle := 0
	log.Print("Starting slot on ", lineName, ", numTicks: ", numTicks, " PumpDutyCycle: ", pumpDutyCycle, " pumpTicks: ", pTicks)
	defer func() {
		log.Print("exiting slot on ", lineName, ", numTicks: ", numTicks, " PumpDutyCycle: ", pumpDutyCycle, " pumpTicks: ", pTicks)
		ticker.Stop()
		time.Sleep(5 * time.Second)
	}()
	var valveOn, pOn bool
	for {
		select {
		case <-ctx.Done():
			goto done
		case <-ticker.C:
			if numTicks > 0 {
				numTicks -= 1
				valveOn = true
				if pTicks > 0 && numTicksInCycle == 0 {
					pTicks -= 1
					pOn = true
					valveOn = false
				} else {
					pOn = false
				}
				numTicksInCycle += 1
				if numTicksInCycle == pumpDutyCycle {
					numTicksInCycle = 0
				}
				tickHandler(valveOn, pOn, lineMapping)
			} else {
				goto done
			}
		}
	}
done:
	tickHandler(false, false, lineMapping)
	wg.Done()
}

// Execute calls go DoSlot when the slot starts
func (s *Schedule) CheckStart(ctx context.Context, wg *sync.WaitGroup, onTarget bool) {
	var handler func(valve, pump1 bool, mapping map[string]int)
	if onTarget {
		handler = TickHandler
	} else {
		handler = OfflineTickHandler
	}
	for _, line := range s.Lines {
		log.Println("processing line", line.LineName)
		for i, slot := range line.Slots {
			if slot.Weekday == "" || time.Now().Weekday().String() == slot.Weekday {
				start, err := slot.GetStartTime()
				if err != nil {
					log.Printf("failed to get start time of slot %d line %s, error: %s\n",
						i, line.LineName, err)
				}
				now := time.Now()
				log.Printf("start hour %d, now hour %d, start minute %d, now minute %d",
					start.Hour(), now.Hour(), start.Minute(), now.Minute())
				if start.Hour() == now.Hour() && start.Minute() == now.Minute() {
					wg.Add(1)
					log.Printf("Doing slot on line %s, slot %v, mapping: %v\n", line.LineName, slot, &line.Mapping)
					go DoSlot(ctx, wg, line.LineName, line.Mapping, slot,
						2000*time.Millisecond,
						handler)
				}
			}
		}
	}
}
