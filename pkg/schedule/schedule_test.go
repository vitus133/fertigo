package schedule_test

import (
	"context"
	"fmt"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/vitus133/fertigo/pkg/schedule"
)

func TestReadScheduleFile(t *testing.T) {
	s, err := schedule.ReadScheduleFile("test_data/test_schedule.yaml")
	assert.Equal(t, err, nil, "failed to read schedule")
	assert.Equal(t, s.Lines[0].LineName, "herbs")

}
func TestWriteScheduleFile(t *testing.T) {
	s, err := schedule.ReadScheduleFile("test_data/test_schedule.yaml")
	assert.Equal(t, err, nil, "failed to read schedule")
	err = s.WriteScheduleFile("/tmp/schedule.yaml")
	assert.Equal(t, err, nil, "failed to write schedule")
}

func TestGetSlot(t *testing.T) {
	s, err := schedule.ReadScheduleFile("test_data/test_schedule.yaml")
	assert.Equal(t, nil, err, "failed to read schedule")
	slot, err := s.GetSlot(0, 0)
	assert.Equal(t, nil, err, "failed to get slot")
	assert.Equal(t, int64(1000000), slot.Duration.Microseconds(), fmt.Sprintf(
		"read %d us instead of 100", slot.Duration.Microseconds()))
}

func TestGetStart(t *testing.T) {
	s, _ := schedule.ReadScheduleFile("test_data/test_schedule.yaml")
	slot, _ := s.GetSlot(0, 0)
	time, err := slot.GetStartTime()
	assert.Equal(t, nil, err, "failed to parse start time")
	t.Log(time)

}

type testData struct {
	valveSlotsSum int
	p1SlotsSum    int
}

func TestDoSlot(t *testing.T) {
	var wg sync.WaitGroup
	ctx, cancel := context.WithCancel(context.Background())
	slot := schedule.FertigationTimeSlot{
		Start:        "",
		Duration:     time.Millisecond * 500,
		PumpDuration: time.Millisecond * 200,
		Weekday:      "",
	}

	var data testData
	wg.Add(1)
	go schedule.DoSlot(ctx, &wg, "test", nil,
		slot,
		time.Millisecond*50,
		func(valve, pump1 bool, mapping map[string]int) {
			if valve {
				data.valveSlotsSum += 1
			}
			if pump1 {
				data.p1SlotsSum += 1
			}
		})
	wg.Wait()
	assert.Equal(t, 10, data.valveSlotsSum, "failed execute slot")
	assert.Equal(t, 4, data.p1SlotsSum, "failed execute slot")
	data = testData{}
	wg.Add(1)
	go schedule.DoSlot(ctx, &wg, "test", nil,
		slot,
		time.Millisecond*50,
		func(valve, pump1 bool, mapping map[string]int) {
			if valve {
				data.valveSlotsSum += 1
			}
			if pump1 {
				data.p1SlotsSum += 1
			}

		})
	time.Sleep(time.Millisecond * 110)
	go cancel()
	wg.Wait()
	assert.Equal(t, 1, data.valveSlotsSum, "failed to cancel slot")
	assert.Equal(t, 1, data.p1SlotsSum, "failed to cancel slot")
}

func TestDoTwoSlots(t *testing.T) {
	var wg sync.WaitGroup
	ctx, cancel := context.WithCancel(context.Background())
	slot1 := schedule.FertigationTimeSlot{
		Start:        "",
		Duration:     time.Millisecond * 600,
		PumpDuration: time.Millisecond * 200,
		Weekday:      "",
	}
	slot2 := schedule.FertigationTimeSlot{
		Start:        "",
		Duration:     time.Millisecond * 400,
		PumpDuration: time.Millisecond * 200,
		Weekday:      "",
	}
	var data testData
	wg.Add(2)
	go schedule.DoSlot(ctx, &wg, "test1", nil,
		slot1,
		time.Millisecond*200,
		func(valve, pump1 bool, mapping map[string]int) {
			if valve {
				data.valveSlotsSum += 1
				fmt.Println("valve1")
			}
			if pump1 {
				data.p1SlotsSum += 1
				fmt.Println("pump1")
			}
		})
	go schedule.DoSlot(ctx, &wg, "test2", nil,
		slot2,
		time.Millisecond*200,
		func(valve, pump1 bool, mapping map[string]int) {
			if valve {
				data.valveSlotsSum += 1
				fmt.Println("valve2")
			}
			if pump1 {
				data.p1SlotsSum += 1
				fmt.Println("pump2")
			}
		})
	wg.Wait()
	cancel()
	fmt.Println(data.p1SlotsSum, data.valveSlotsSum)
	assert.Equal(t, 2, data.p1SlotsSum)
	assert.Equal(t, 5, data.valveSlotsSum)
}

func TestDoTwoSlotsCancel(t *testing.T) {
	var wg sync.WaitGroup
	ctx, cancel := context.WithCancel(context.Background())
	slot1 := schedule.FertigationTimeSlot{
		Start:        "",
		Duration:     time.Millisecond * 600,
		PumpDuration: time.Millisecond * 100,
		Weekday:      "",
	}
	slot2 := schedule.FertigationTimeSlot{
		Start:        "",
		Duration:     time.Millisecond * 400,
		PumpDuration: time.Millisecond * 100,
		Weekday:      "",
	}
	wg.Add(1)
	wg.Add(1)
	var data testData
	go schedule.DoSlot(ctx, &wg, "test1", nil,
		slot1,
		time.Millisecond*20,
		func(valve, pump1 bool, mapping map[string]int) {
			if valve {
				data.valveSlotsSum += 1
				fmt.Println("valve1")
			}
			if pump1 {
				data.p1SlotsSum += 1
				fmt.Println("pump1")
			}
		})
	go schedule.DoSlot(ctx, &wg, "test2", nil,
		slot2,
		time.Millisecond*20,
		func(valve, pump1 bool, mapping map[string]int) {
			if valve {
				data.valveSlotsSum += 1
				fmt.Println("valve2")
			}
			if pump1 {
				data.p1SlotsSum += 1
				fmt.Println("pump2")
			}
		})
	time.Sleep(time.Millisecond * 30)
	cancel()
	wg.Wait()

	fmt.Println(data.p1SlotsSum, data.valveSlotsSum)
	assert.Condition(t, func() bool {
		return data.p1SlotsSum <= 2
	}, "slots cancel - pump worked too long")
	assert.Equal(t, 0, data.valveSlotsSum,
		"slots cancel - valve worked too long")

}
