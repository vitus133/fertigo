package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/namsral/flag"
	rpio "github.com/stianeikeland/go-rpio/v4"
	"gitlab.com/vitus133/fertigo/pkg/gitconf"
	"gitlab.com/vitus133/fertigo/pkg/schedule"
	"golang.org/x/sync/semaphore"
)

const defaultTick = 30 * time.Second

type config struct {
	tick              time.Duration
	repoPath          string
	branch            string
	repository        string
	localSchedulePath string
}

func (c *config) init(args []string) error {
	flags := flag.NewFlagSet(args[0], flag.ExitOnError)
	flags.String(flag.DefaultConfigFlagname, "", "Path to config file")

	var (
		tick              = flags.Duration("tick", defaultTick, "Ticking interval")
		repository        = flags.String("repository", "https://gitlab.com/vitus133/fertigo", "Git repository url")
		repoPath          = flags.String("repoPath", "schedule/balcony.yaml", "Schedule file path in the repository")
		branch            = flags.String("branch", "main", "Repository branch")
		localSchedulePath = flags.String("localSchedulePath", "./schedule.yaml", "Path of schedule local version")
	)

	if err := flags.Parse(args[1:]); err != nil {
		return err
	}

	c.tick = *tick
	c.repository = *repository
	c.repoPath = *repoPath
	c.branch = *branch
	c.localSchedulePath = *localSchedulePath

	return nil
}

func main() {
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, os.Interrupt, syscall.SIGHUP)
	stopChan := make(chan bool, 1)
	c := &config{}

	defer func() {
		signal.Stop(signalChan)
	}()

	go func() {
		for {
			select {
			case s := <-signalChan:
				switch s {
				case syscall.SIGHUP:
					c.init(os.Args)
				case os.Interrupt:
					stopChan <- true
				}
			}
		}
	}()

	if err := run(c, os.Stdout, stopChan); err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}
}

func resyncScheduleFile(ctx context.Context, sem *semaphore.Weighted, c *config) {
	s, err := gitconf.PullSchedule(c.repository, c.branch, c.repoPath)
	if err != nil {
		log.Println("can't sync schedule file")
		return
	}
	if err := sem.Acquire(ctx, 1); err != nil {
		log.Printf("resyncScheduleFile: failed to acquire semaphore: %v\n", err)
	}
	err = s.WriteScheduleFile(c.localSchedulePath)
	sem.Release(1)
	if err != nil {
		log.Println("failed to store local copy of schedule")
	}
}

func ensureScheduleFile(c *config) schedule.Schedule {
	for {
		s, err := schedule.ReadScheduleFile(c.localSchedulePath)
		if err != nil {
			s, err = gitconf.PullSchedule(c.repository, c.branch, c.repoPath)
			if err != nil {
				log.Println("failed to read remote schedule, will retry")
				time.Sleep((c.tick))
				continue
			}
			err = s.WriteScheduleFile(c.localSchedulePath)
			if err != nil {
				log.Println("failed to store local copy of schedule, will continue")
				return *s
			}
		}
		return *s
	}
}

func run(c *config, stdout io.Writer, stopChan chan bool) error {
	c.init(os.Args)
	log.SetOutput(os.Stdout)
	log.Println("Starting fertigo daemon")
	os.Remove(c.localSchedulePath)
	s := ensureScheduleFile(c)
	ctxFileAccess := context.TODO()
	sem := semaphore.NewWeighted(int64(1))
	resyncTicker := time.NewTicker(c.tick)
	var onTarget bool
	// Check whether running on target
	if _, err := os.Stat("/sys/class/gpio"); err == nil {
		onTarget = true
		for _, line := range s.Lines {
			err = schedule.InitIo(line.Mapping)
			if err != nil {
				return fmt.Errorf("failed to initialize io, %s", err)
			}
			defer rpio.Close()
		}
	}
	jobStart := time.After(schedule.NowToMinute())

	var wg sync.WaitGroup
	ctx, cancel := context.WithCancel(context.Background())

	for {
		select {
		case <-resyncTicker.C:
			log.Println("Resync schedule")
			go resyncScheduleFile(ctxFileAccess, sem, c)
		case <-jobStart:
			if err := sem.Acquire(ctxFileAccess, 1); err != nil {
				log.Printf("failed to acquire semaphore: %v\n", err)
			}
			s, err := schedule.ReadScheduleFile(c.localSchedulePath)
			sem.Release(1)
			if err != nil {
				log.Println("failed to read schedule file")
			}
			log.Println("Execute schedule")
			s.CheckStart(ctx, &wg, onTarget)
			jobStart = time.After(schedule.NowToMinute())
		case <-stopChan:
			cancel()
			wg.Wait()
			return fmt.Errorf("cancelled all jobs, exiting")

		}
	}
}
